import numpy as np

class Face:
    def __init__(self, points):
        self.pointID = points
        self.owner = None
        self.neighbour = None

    def __getitem__(self, index):
        return self.pointID[index]

