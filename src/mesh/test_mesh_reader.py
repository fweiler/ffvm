import mesh_reader
import unittest
from Face import Face
from Point import Point

"""
Test function class mesh reader
"""
class TestMeshReader(unittest.TestCase):
    def setUp(self):
        case = "/home/felix/OpenFOAM/felix-7/run/mesh/blockMesh/pipe"
        reader = mesh_reader.MeshReader(case)
        self.mesh = reader.mesh

    def test_type_point_is_float(self):
        point = self.mesh.points[0]
        self.assertEqual(type(point), float)

    def test_type_face_is_int(self):
        face = self.mesh.faces[0]
        self.assertEqual(type(face[0]), int)

    def test_type_owner_is_int(self):
        owner = self.mesh.owner[0]
        self.assertEqual(type(owner), int)


    def test_length_faces_is_correct(self):
        self.assertEqual(self.mesh.faces.length, len(self.mesh.faces[:]))


"""
Test data class Point
"""
class TestFaceClass(unittest.TestCase):
    def setUp(self):
        self.points = [0, 1, 2, 3, 4]
        self.face = Face(self.points)

    def test_is_subscriptable(self):
        self.assertEqual(self.face[0], self.points[0])
        

"""
Test data class Point
"""
class TestPointClass(unittest.TestCase):
    def setUp(self):
        self.vertices = [0.0, 1.0, 1.0]
        self.point = Point(*self.vertices)

    def test_is_subscriptable(self):
        self.assertEqual(self.point[0], self.vertices[0])
        

def main():
    case = "/home/felix/OpenFOAM/felix-7/run/mesh/blockMesh/pipe"
    reader = mesh_reader.MeshReader(case)
    mesh = reader.mesh
    print(mesh.faces[0].points)
    # print("hi from main")
    #reader.read_header("constant/polyMesh/points")


if __name__ == "__main__":
    unittest.main()
