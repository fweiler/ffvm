import numpy as np

class Point:
    def __init__(self, x, y, z):
        self._data = np.array([x, y, z])

    def __getitem__(self, index):
        if index >= 3 or index < 0:
            raise IndexError
        return self._data[index]

