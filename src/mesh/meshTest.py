import numpy as np

Nx, Ny, Nz = 10, 10, 10
dx, dy, dz = 1, 1, 1
    
def ijk_to_n(self, i, j, k):
    # TODO: check if i < Nx, j < Ny, k < Nz
    n = i + Nx*j + Nx*Ny*k
    return n
    
def C(self, i, j, k):
    return cells[self.ijk_to_n(i,j,k), :]
    
def n_to_ijk(self, n):
    # TODO: check if n < (Nx * Ny * Nz)
    k = n // Nx*Ny
    j = (n - k*Nx*Ny) // Nx
    i = n - Nx*j - Nx*Ny*k
    return i, j, k


#cells = np.zeros([Nx*Ny*Nz, 3])
#faces = np.zeros([(Nx+1)*(Ny+1)*(Nz+1)])

initialCondition = 0
boundaryWest = 1

cells = np.empty(Nx*Ny*Nz, dtype=object)
faces = np.empty((Nx+1)*(Ny+1)*(Nz+1), dtype=object)
phi = np.zeros(Nx*Ny*Nz, dtype=float)
phi_f = np.zeros((Nx+1)*(Ny+1)*(Nz+1), dtype=float)

# Generate mesh
for i in range(Nx):
    for j in range(Ny):
        for k in range(Nz):
            n = ijk_to_n(i, j, k)
            cells[n] = StructuredCell(i, j, k)
            phi[n] = initialCondition

for i in range(Nx):
    for j in range(Ny):
        for k in range(Nz):
            n = ijk_to_n
            phi_f[n(i,j,k)] = 0.5 * phi[n(i,j,k)] + 
            phi[n] = 

# Set initial and boundary conditions
def setBoundary():
    for i in range(Nx):
        for j in range(Ny):
            phi_f(ijk_to_n(i, j, 0)) = bottom
            phi_f(ijk_to_n(i, j, Nz)) = top

    for i in range(Nx):
        for k in range(Nz):
            phi_f(ijk_to_n(i, 0, k)) = south
            phi_f(ijk_to_n(i, Ny, k)) = north

    for j in range(Ny):
        for k in range(Nz):
            phi_f(ijk_to_n(0, j, k)) = west
        phi_f(ijk_to_n(Nx, j, k)) = east

class StructuredCell(Cell):
    def __init__(self, i, j, k):
        self.i = i
        self.j = j
        self.k = k


class Cell:
    def __init__(self):
        self.n = 0
        self.faces = list()


class Face:
    def __init__(self):
        self.n = 0
        self.owner = None
        self.neighbour = None

def main():
    mesh = StructuredMesh(3, 4, 2, 0.1, 0.1, 0.1)
    print(np.linalg.norm(mesh.C(0,0,0) - mesh.C(1,1,1)))

if __name__ == "__main__":
    main()
