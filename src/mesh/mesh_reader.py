import numpy as np
import os
import re

from Mesh import Mesh
from Face import Face


class MeshReader():
    def __init__(self, case_path):
        self.case = case_path

        # Read mesh from files
        self.mesh = self.read_mesh()

        # Create cells etc


    def read_faces(self):
        faceList = []
        length = 0
        for point_list in self.read_file('constant/polyMesh/faces', int):
            if not length:
                length = point_list
            else:
                # Split entry into length (first entry) and point list
                n_points = point_list[0]
                point_list = point_list[1:]
                assert n_points == len(point_list)
                faceList.append(Face(point_list))

        assert length == len(faceList)
        return faceList


    def read_cells(self):
        owner = self.read_file('constant/polyMesh/owner', int)
        neighbour = self.read_file('constant/polyMesh/neighbour', int)

        # Need to know the number of cells a priori
        # Iterate over all faces
        # Add face to owner and neighbour cell


    def read_mesh(self):
        points = self.read_file('constant/polyMesh/points', float)
        faces = self.read_faces()
        owner = self.read_file('constant/polyMesh/owner', int)
        neighbour = self.read_file('constant/polyMesh/neighbour', int)

        return Mesh(points, faces, owner, neighbour)


    def read_file(self, filename, conversion):
        path = os.path.join(self.case, filename)
        header_read = False

        data = []

        # TODO: Add binary read
        with open(path, 'r') as f:
            for line in f:
                if line.startswith("FoamFile"):
                    foamFileDict = read_dict(f)
                    header_read = True
                if header_read == True:
                    data = read_list(f, conversion)

            return data


def read_dict(fileHandler) -> dict:
    myDict = {}
    for line in fileHandler:
        # Stop reading if closed brackets
        if ("}" in line):
            break
        if ("{" in line):
            continue
        else:
            words = line.split()
            value = str(words[1].strip(";"))
            try:
                value = float(value)
            except:
                pass

            myDict[words[0]] = value

    return myDict



def read_list(fileHandler, conversion):
    pattern = re.compile(r"\d+\.*\d*")
    array = list()

    for line in fileHandler:
        row = pattern.findall(line)
        # If matching results found
        if row:
            # For single value rows, resulting array should be 1d
            if len(row) == 1:
                array.append(conversion(row[0]))
            # Otherwise, resulting array is 2d
            else:
                array.append(list(map(conversion, row)))

    return array

