import Face

class FaceList:
    def __init__(self, length, faces):
        self.length = length
        self.faces = faces 
        assert len(self.faces) == self.length, "Wrong input!"

    
    def __getitem__(self, index):
        return self.faces[index]


    def append(self, face):
        self.length += 1
        self.faces.append(face)


