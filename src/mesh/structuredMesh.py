import numpy as np

class StructuredMesh:
    def __init__(self, Nx, Ny, Nz, dx, dy, dz):
        self.Nx, self.Ny, self.Nz = Nx, Ny, Nz
        self.dx, self.dy, self.dz = dx, dy, dz
        self.generateArray()

    
    def ijk_to_n(self, i, j, k):
        Nx, Ny, Nz = self.Nx, self.Ny, self.Nz
        n = i + Nx*j + Nx*Ny*k
        return n
    
    def C(self, i, j, k):
        return self.cells[self.ijk_to_n(i,j,k), :]
    
    def n_to_ijk(self, n):
        Nx, Ny, Nz = self.Nx, self.Ny, self.Nz
        k = n // Nx*Ny
        j = (n - k*Nx*Ny) // Nx
        i = n - Nx*j - Nx*Ny*k
        return i, j, k


    def generateArray(self):
        self.cells = np.zeros([self.Nx*self.Ny*self.Nz, 3])
        for i in range(self.Nx):
            for j in range(self.Ny):
                for k in range(self.Nz):
                    n = self.ijk_to_n(i, j, k)
                    self.cells[n, :] = np.array([i*self.dx, j*self.dy, k*self.dz])


def main():
    mesh = StructuredMesh(3, 4, 2, 0.1, 0.1, 0.1)
    print(np.linalg.norm(mesh.C(0,0,0) - mesh.C(1,1,1)))

if __name__ == "__main__":
    main()
